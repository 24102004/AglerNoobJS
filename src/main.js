const LineAPI = require('./api');
const request = require('request');
const fs = require('fs');
const unirest = require('unirest');
const webp = require('webp-converter');
const path = require('path');
const rp = require('request-promise');
const config = require('./config');
const { Message, OpType, Location } = require('../curve-thrift/line_types');
//let exec = require('child_process').exec;

class LINE extends LineAPI {
    constructor() {
        super();
        this.receiverID = '';
        this.checkReader = [];
        this.spamName = [];
        this.stateStatus = {
            cancel: 0,
            kick: 0,
        };
        this.messages;
        this.payload;
        this.stateUpload =  {
                file: '',
                name: '',
                group: '',
                sender: ''
            }
    }

    get payload() {
        if(typeof this.messages !== 'undefined'){
            return (this.messages.text !== null) ? this.messages.text.split(' ').splice(1) : '' ;
        }
        return false;
    }

    get myBot() {
        const bot = ['uc4670540ac3b113403a9667df0b09df3','u13d642653d06fa3ec3e946082023a09a','u6ae5b6311554bbebd6234868967afb7b','u28bee33a596b0fafb73d601397321c22','ude169d400418151420e1e8b6974f4dd5','uf15f63d71ad0162c3e791cd3c6efca96','u3afe2f2262f043a1818b4ed72134b077','u4f2e22c2c6ffa04b7a8337ecc6ba4da2','u528c62667d965b0227469ac46850f8db'];
        return bot; 
    }

    isAdminOrBot(param) {
        return this.myBot.includes(param);
    }

    getOprationType(operations) {
        for (let key in OpType) {
            if(operations.type == OpType[key]) {
                if(key !== 'NOTIFIED_UPDATE_PROFILE') {
                    console.info(`[* ${operations.type} ] ${key} `);
                }
            }
        }
    }

    poll(operation) {
        if(operation.type == 25 || operation.type == 26) {
            let message = new Message(operation.message);
            this.receiverID = message.to = (operation.message.to === this.myBot[0]) ? operation.message.from : operation.message.to ;
            Object.assign(message,{ ct: operation.createdTime.toString() });
            this.textMessage(message)
        }

        if(operation.type == 13 && this.stateStatus.cancel == 1) {
            this._cancel(operation.param2,operation.param1);
            
        }

        if(operation.type == 11 && !this.isAdminOrBot(operation.param2) && this.stateStatus.qrp == 1) {
            this._kickMember(operation.param1,[operation.param2]);
            this.messages.to = operation.param1;
            this.qrOpenClose();
        }

        if(operation.type == 19) { //ada kick
     {
let nadyaq = new Message();
nadyaq.to = operation.param1;
nadyaq.text = "WOI!! JANGAN KICK SEMBARANGAN NGENTOT GUA KICK NIECH?!!"
this._client.sendMessage(0,nadyaq);
     }
            // op1 = group nya
            // op2 = yang 'nge' kick
            // op3 = yang 'di' kick

            if(!this.isAdminOrBot(operation.param2)){
                this._kickMember(operation.param1,[operation.param2]);
            } 
            if(!this.isAdminOrBot(operation.param3)){
                this._invite(operation.param1,[operation.param3]);
            }

        }

if(operation.type == 11) { //bukattupQR
     {
let nadyasayang = new Message();
nadyasayang.to = operation.param1;
nadyasayang.text = "Jangan Ubah Gambar Dan Nama Group/Jangan Dimainin QR-nya Bangsat Gua Kick Niech!"+"Cuma Admin Yang Bisa Kontol!!!"
this._client.sendMessage(0,nadyasayang);
     }
            if(!this.isAdminOrBot(operation.param2)){
                this._kickMember(operation.param1,[operation.param2]);
            } 
}

if(operation.type == 15) { //leave grup
     {
let nadyasayang = new Message();
nadyasayang.to = operation.param1;
nadyasayang.text = "Bye tot~ Selamat jalan semoga tidak coli lagi~"

this._client.sendMessage(0,nadyasayang);
     }
  this._invite(operation.param1,[operation.param2]);
}

           if(operation.type == 16){
 // botjoingroup
{
let nadyaq = new Message();
nadyaq.to = operation.param1;
nadyaq.text = "Terimakasih sudah mengundang saya ke grup!"
,"Ketik (help) tanpa tanda kurung jika ingin melihat command saya."

this._client.sendMessage(0,nadyaq);
}
{
let nadya = new Message();
nadya.to = operation.param1;
nadya.text = "Ketik (help) tanpa tanda kurung jika ingin melihat command saya."

this._client.sendMessage(0,nadya);
  }
}

if(operation.type == 32) { //adaygbatalin
let nadyaq = new Message();
nadyaq.to = operation.param1;
nadyaq.text = "Kok Dibatalin? Emangnya Dia Siapa?"

this._client.sendMessage(0,nadyaq);
     }

        if(operation.type == 55){ //ada reader
            const idx = this.checkReader.findIndex((v) => {
                if(v.group == operation.param1) {
                    return v
                }
            })
            if(this.checkReader.length < 1 || idx == -1) {
                this.checkReader.push({ group: operation.param1, users: [operation.param2], timeSeen: [operation.param3] });
            } else {
                for (var i = 0; i < this.checkReader.length; i++) {
                    if(this.checkReader[i].group == operation.param1) {
                        if(!this.checkReader[i].users.includes(operation.param2)) {
                            this.checkReader[i].users.push(operation.param2);
                            this.checkReader[i].timeSeen.push(operation.param3);
                        }
                    }
                }
            }
        }


        if(operation.type == 5) { // diadd
let nadyaq = new Message();
nadyaq.to = operation.param1;
nadyaq.text = "Thanks for add me!\n\nPowered by ✍T҉̶̘̟̼̉̈́͐͋͌̊Σ̶Δ̶M҉̶̘͈̺̪͓̺ͩ͂̾ͪ̀̋ ̶̶̶N⃟̶O̺͆O̶̶͓̽B⃟ β̶Ω̶T҉̶̘̟̼̉̈́͐͋͌̊☞"
this._client.sendMessage(0,nadyaq);
}

        if(operation.type == 13) { // diinvite
                this._acceptGroupInvitation(operation.param1);
let nadyaq = new Message();
nadyaq.to = operation.param1;
nadyaq.text = "Jangan Main Invite Sembarangan Ngentot !"

this._client.sendMessage(0,nadyaq);
}
        this.getOprationType(operation);
    }

    command(msg, reply) {
        if(this.messages.text !== null) {
            if(this.messages.text === msg.trim()) {
                if(typeof reply === 'function') {
                    reply();
                    return;
                }
                if(Array.isArray(reply)) {
                    reply.map((v) => {
                        this._sendMessage(this.messages, v);
                    })
                    return;
                }
                return this._sendMessage(this.messages, reply);
            }
        }
    }

    async getProfile() {
        let { displayName } = await this._myProfile();
        return displayName;
    }


    async cancelMember() {
        let groupID;
        if(this.payload.length > 0) {
            let [ groups ] = await this._findGroupByName(this.payload.join(' '));
            groupID = groups.id;
        } 
        let gid = groupID || this.messages.to;
        let { listPendingInvite } = await this.searchGroup(gid);
        if(listPendingInvite.length > 0){
            this._cancel(gid,listPendingInvite);
        }
    }

    async searchGroup(gid) {
        let listPendingInvite = [];
        let thisgroup = await this._getGroups([gid]);
        if(thisgroup[0].invitee !== null) {
            listPendingInvite = thisgroup[0].invitee.map((key) => {
                return key.mid;
            });
        }
        let listMember = thisgroup[0].members.map((key) => {
            return { mid: key.mid, dn: key.displayName };
        });

        return { 
            listMember,
            listPendingInvite
        }
    }

    OnOff() {
        if(this.isAdminOrBot(this.messages.from)){
            let [ actions , status ] = this.messages.text.split(' ');
            const action = actions.toLowerCase();
            const state = status.toLowerCase() == 'on' ? 1 : 0;
            this.stateStatus[action] = state;
            this._sendMessage(this.messages,`Status: \n${JSON.stringify(this.stateStatus)}`);
        } else {
            this._sendMessage(this.messages,`Lo Bukan Admin Ngentot, Mau Jadi Admin? Chat Admin1`);
            this._sendMessage(this.messages,`Ketik Keyword Ini Untuk Melihat Admin Anjink:\nAdmin1\nAdmin2\nAdmin3\nAdmin4\nAdmin5\nAdmin6\nAdmin7\nAdmin8\nAdmin9\nAdmin10\nAdmin11\nAdmin12\nAdmin13\nAdmin14\nAdmin15`);
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
    }

    mention(listMember) {
        let mentionStrings = [''];
        let mid = [''];
        for (var i = 0; i < listMember.length; i++) {
            mentionStrings.push('@'+listMember[i].displayName+'\n');
            mid.push(listMember[i].mid);
        }
        let strings = mentionStrings.join('');
        let member = strings.split('@').slice(1);
        
        let tmp = 0;
        let memberStart = [];
        let mentionMember = member.map((v,k) => {
            let z = tmp += v.length + 1;
            let end = z - 1;
            memberStart.push(end);
            let mentionz = `{"S":"${(isNaN(memberStart[k - 1] + 1) ? 0 : memberStart[k - 1] + 1 ) }","E":"${end}","M":"${mid[k + 1]}"}`;
            return mentionz;
        })
        return {
            names: mentionStrings.slice(1),
            cmddata: { MENTION: `{"MENTIONEES":[${mentionMember}]}` }
        }
    }

    async leftGroupByName(name) {
        let payload = name || this.payload.join(' ');
        let gid = await this._findGroupByName(payload);
        for (let i = 0; i < gid.length; i++) {
            this._leaveGroup(gid[i].id);
        }
        return;
    }

    async recheck(cs,group) {
        let users;
        for (var i = 0; i < cs.length; i++) {
            if(cs[i].group == group) {
                users = cs[i].users;
            }
        }
        
        let contactMember = await this._getContacts(users);
        return contactMember.map((z) => {
                return { displayName: z.displayName, mid: z.mid };
            });
    }

    removeReaderByGroup(groupID) {
        const groupIndex = this.checkReader.findIndex(v => {
            if(v.group == groupID) {
                return v
            }
        })

        if(groupIndex != -1) {
            this.checkReader.splice(groupIndex,1);
        }
    }

    async getSpeed() {
        let curTime = Date.now() / 30000;
        await this._sendMessage(this.messages, 'Wait goblok...');
        const rtime = (Date.now() / 30000) - curTime;
        await this._sendMessage(this.messages, `${rtime} Second`);
        return;
    }
    

    async tagall() {
        let rec = await this._getGroup(this.messages.to);
        const mentions = await this.mention(rec.members);
        this.messages.contentMetadata = mentions.cmddata;
        await this._sendMessage(this.messages,mentions.names.join(''));
        return;
    }
    

    async tagall2() {
        let rec = await this._getGroup(this.messages.to);
        const mentions = await this.mention(rec.members);
        this.messages.contentMetadata = mentions.cmddata;
        await this._sendMessage(this.messages,mentions.names.join(''));
        return;
    }

    vn() {
        this._sendFile(this.messages,`${__dirname}/../download/${this.payload.join(' ')}.m4a`,3);
    }

    lagu() {
     {
        this._sendFile(this.messages,`${__dirname}/../download/${this.payload.join(' ')}.mp3`,3);
    }
    {
        this._sendMessage(this.messages, `Ok, Sabar Ya Tot, Tungguin... Lagunya Lagi Gua Prosses Anj`);
    }
    }

    video() {
    {
        this._sendFile(this.messages,`${__dirname}/../download/${this.payload.join(' ')}.mp4`,2);
    }
         this._sendMessage(this.messages, `Ok, Sabar Ya Tot, Tungguin... Videonya Lagi Gua Prosses Anj`);
    }

    checkKernel() {
        exec('uname -a',(err, sto) => {
            if(err) {
                this._sendMessage(this.messages, err);
                return
            }
            this._sendMessage(this.messages, sto);
            return;
        });
    }

    setReader() {
        this._sendMessage(this.messages, "↔️↔️↔️↔️ CCTV AKTIF TOT↔️↔️↔️↔️️"+
"Ketik Cyduk Untuk Melihat Sider Anj!");
        this.removeReaderByGroup(this.messages.to);
        return;
    }

    keluar() {
       {            this._sendMessage(this.messages, `Lo Mau Ngusir Gua Ya Bangsat!!??? :(`);
      }
      {
                    this._sendMessage(this.messages, `Ketik "#ya" Atau "#tidak"`);
      }
            return;
      }

    batal() {
                   this._sendMessage(this.messages, `Yes, Makasih Udah Ga Jadi Ngusir Gua Makanya Jan Suka Coli Lagi Anj`);
      }


    spam2() {
                    this._sendMessage(this.messages, `3`);
                    this._sendMessage(this.messages, `2`);
                    this._sendMessage(this.messages, `1`);
                    this._sendMessage(this.messages, `Fuck Off`);
                    this._sendMessage(this.messages, `Ku mengejar bus yang mulai berjalan`);
                    this._sendMessage(this.messages, `Ku ingin ungkapkan kepada dirimu`);
                    this._sendMessage(this.messages, `Kabut dalam hatiku telah menghilang`);
                    this._sendMessage(this.messages, `Dan hal yang penting bagiku pun terlihat`);
                    this._sendMessage(this.messages, `Walaupun jawaban itu sebenarnya begitu mudah`);
                    this._sendMessage(this.messages, `Tetapi entah mengapa diriku melewatkannya`);
                    this._sendMessage(this.messages, `Untukku menjadi diri sendiri`);
                    this._sendMessage(this.messages, `Ku harus jujur, pada perasaanku`);
                    this._sendMessage(this.messages, `Ku suka dirimu ku suka`);
                    this._sendMessage(this.messages, `Ku berlari sekuat tenaga`);
                    this._sendMessage(this.messages, `Ku suka selalu ku suka`);
                    this._sendMessage(this.messages, `Ku teriak sebisa suaraku`);
                    this._sendMessage(this.messages, `Ku suka dirimu ku suka`);
                    this._sendMessage(this.messages, `Walau susah untukku bernapas`);
                    this._sendMessage(this.messages, `Tak akan ku sembunyikan`);
                    this._sendMessage(this.messages, `Oogoe daiyamondo~`);
                    this._sendMessage(this.messages, `Saat ku sadari sesuatu menghilang`);
                    this._sendMessage(this.messages, `Hati ini pun resah tidak tertahankan`);
                    this._sendMessage(this.messages, `Sekarang juga yang bisa ku lakukan`);
                    this._sendMessage(this.messages, `Merubah perasaan ke dalam kata kata`);
                    this._sendMessage(this.messages, `Mengapa sedari tadi`);
                    this._sendMessage(this.messages, `Aku hanya menatap langit`);
                    this._sendMessage(this.messages, `Mataku berkaca kaca`);
                    this._sendMessage(this.messages, `Berlinang tak bisa berhenti`);
                    this._sendMessage(this.messages, `Di tempat kita tinggal, didunia ini`);
                    this._sendMessage(this.messages, `Dipenuhi cinta, kepada seseorang`);
                    this._sendMessage(this.messages, `Ku yakin ooo ku yakin`);
                    this._sendMessage(this.messages, `Janji tak lepas dirimu lagi`);
                    this._sendMessage(this.messages, `Ku yakin ooo ku yakin`);
                    this._sendMessage(this.messages, `Akhirnya kita bisa bertemu`);
                    this._sendMessage(this.messages, `Ku yakin ooo ku yakin`);
                    this._sendMessage(this.messages, `Ku akan bahagiakan dirimu`);
                    this._sendMessage(this.messages, `Ku ingin kau mendengarkan`);
                    this._sendMessage(this.messages, `Oogoe daiyamondo~`);
                    this._sendMessage(this.messages, `Jika jika kamu ragu`);
                    this._sendMessage(this.messages, `Takkan bisa memulai apapun`);
                    this._sendMessage(this.messages, `Ungkapkan perasaanmu`);
                    this._sendMessage(this.messages, `Jujurlah dari sekarang juga`);
                    this._sendMessage(this.messages, `Jika kau bersuar`);
                    this._sendMessage(this.messages, `Cahaya kan bersinar`);
                    this._sendMessage(this.messages, `Ku suka dirimu ku suka`);
                    this._sendMessage(this.messages, `Ku berlari sekuat tenaga`);
                    this._sendMessage(this.messages, `Ku suka selalu ku suka`);
                    this._sendMessage(this.messages, `Ku teriak sebisa suaraku`);
                    this._sendMessage(this.messages, `Ku suka dirimu ku suka`);
                    this._sendMessage(this.messages, `Sampaikan rasa sayangku ini`);
                    this._sendMessage(this.messages, `Ku suka selalu ku suka`);
                    this._sendMessage(this.messages, `Ku teriakkan ditengah angin`);
                    this._sendMessage(this.messages, `Ku suka dirimu ku suka`);
                    this._sendMessage(this.messages, `Walau susah untuk ku bernapas`);
                    this._sendMessage(this.messages, `Tak akan ku sembunyikan`);
                    this._sendMessage(this.messages, `Oogoe daiyamondo~`);
                    this._sendMessage(this.messages, `Katakan dengan berani`);
                    this._sendMessage(this.messages, `Jika kau diam kan tetap sama`);
                    this._sendMessage(this.messages, `Janganlah kau merasa malu`);
                    this._sendMessage(this.messages, `“Suka” itu kata paling hebat!`);
                    this._sendMessage(this.messages, `“Suka” itu kata paling hebat!`);
                    this._sendMessage(this.messages, `“Suka” itu kata paling hebat!`);
                    this._sendMessage(this.messages, `Ungkapkan perasaanmu`);
                    this._sendMessage(this.messages, `Jujurlah dari sekarang juga..`);
                    this._sendMessage(this.messages, `SPAM IS DONE`);
           return;
    }

    clear() {
        this._sendMessage(this.messages, `List Sider Terhapus !`);
        this.checkReader = [];
        return
    }

    list() {
            this._sendMessage(this.messages,`Ketik Keyword Ini Untuk Melihat Admin : Admin1                      Admin2                      Admin3                      Admin4                      Admin5                      Admin6                      Admin7                      Admin8                      Admin9                      Admin10                     Admin11                     Admin12                     Admin13                     Admin14                     Admin15`);
     }

creator() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'uc4670540ac3b113403a9667df0b09df3'}
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }

admin1() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'uc4670540ac3b113403a9667df0b09df3'}
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }

admin2() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u15848486613e6828fcbd1f94c76e1572' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }

admin3() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u3afe2f2262f043a1818b4ed72134b077' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }

admin4() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'uc7d319b7d2d38c35ef2b808e3a2aeed9' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }

admin5() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u13d642653d06fa3ec3e946082023a09a' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }

admin6() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'ude169d400418151420e1e8b6974f4dd5' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }

admin7() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u2b002b6d9738337dfa382dcd0bd23be3' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }

admin8() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u3afe2f2262f043a1818b4ed72134b077' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }

admin9() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'uc4670540ac3b113403a9667df0b09df3' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
}

admin10() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u6ae5b6311554bbebd6234868967afb7b' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
}

admin11() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u28bee33a596b0fafb73d601397321c22' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
}

admin12() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u3afe2f2262f043a1818b4ed72134b077' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
}

admin13() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'uc7d319b7d2d38c35ef2b808e3a2aeed9' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
}

admin14() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u9025557127eaca9b2ddd068e2040c6ce' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
}

admin15() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'ude169d400418151420e1e8b6974f4dd5' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
}

    admin16() {
                    this._sendMessage(this.messages, `Admin 16 Belom Ada`);
     }

    admin17() {
                    this._sendMessage(this.messages, `Admin 17 Belom Ada`);
     }

    admin18() {
                    this._sendMessage(this.messages, `Admin 18 Belom Ada`);
     }

    admin19() {
                    this._sendMessage(this.messages, `Admin 19 Belom Ada`);
     }

    admin20() {
                    this._sendMessage(this.messages, `Admin 20 Belom Ada`);
     }

bot2() {
        let msg = {
            text:null,
            contentType: 13,
            contentPreview: null,
            contentMetadata: 
            { mid: 'u3afe2f2262f043a1818b4ed72134b077' }
        }
        Object.assign(this.messages,msg);
        this._sendMessage(this.messages);
 }


    resetStateUpload() {
        this.stateUpload = {
            file: '',
            name: '',
            group: '',
            sender: ''
        };
    }

    prepareUpload() {
        this.stateUpload = {
            file: true,
            name: this.payload.join(' '),
            group: this.messages.to,
            sender: this.messages.from
        };
        this._sendMessage(this.messages,` ${this.stateUpload.name}`);
        return;
    }
    
    async doUpload({ id, contentType }) {
        let url = `https://obs-sg.line-apps.com/talk/m/download.nhn?oid=${id}`;
        await this._download(url,this.stateUpload.name, contentType);
        this.messages.contentType = 0;
        this._sendMessage(this.messages,` ${this.stateUpload.name} `);
        this.resetStateUpload()
        return;
    }

    searchLocalImage() {
        let name = this.payload.join(' ');
        let dirName = `${__dirname}/../download/${name}.jpg`;
        try {
            this._sendImage(this.messages,dirName);
        } catch (error) {
             this._sendImage(this.messages,`No Photo #${name} Uploaded `);
        }
        return ;
        
    }

    async joinQr() {
        const [ ticketId ] = this.payload[0].split('g/').splice(-1);
        let { id } = await this._findGroupByTicket(ticketId);
        await this._acceptGroupInvitationByTicket(id,ticketId);
        return;
    }

    async qrOpenClose() {
        let updateGroup = await this._getGroup(this.messages.to);
        updateGroup.preventJoinByTicket = true;
        if(typeof this.payload !== 'undefined') {
            let [ type ] = this.payload;

            if(type === 'open') {
                updateGroup.preventJoinByTicket = false;
                const groupUrl = await this._reissueGroupTicket(this.messages.to)
                this._sendMessage(this.messages,`Line group = line://ti/g/${groupUrl}`);
            }
        }
        await this._updateGroup(updateGroup);
        return;
    }

    spamGroup() {
        if(this.isAdminOrBot(this.messages.from) && this.payload[0] !== 'kill') {
            let s = [];
            for (let i = 0; i < this.payload[1]; i++) {
                let name = `${Math.ceil(Math.random() * 1000)}${i}`;
                this.spamName.push(name);
                this._createGroup(name,[this.payload[0]]);
            }
            return;
        } 
        for (let z = 0; z < this.spamName.length; z++) {
            this.leftGroupByName(this.spamName[z]);
        }
        return true;
    }

    checkIP() {
        exec(`wget ipinfo.io/${this.payload[0]} -qO -`,(err, res) => {
            if(err) {
                this._sendMessage(this.messages,'Error Please Install Wget');
                return 
            }
            const result = JSON.parse(res);
            if(typeof result.error == 'undefined') {
                const { org, country, loc, city, region } = result;
                try {
                    const [latitude, longitude ] = loc.split(',');
                    let location = new Location();
                    Object.assign(location,{ 
                        title: `Location:`,
                        address: `${org} ${city} [ ${region} ]\n${this.payload[0]}`,
                        latitude: latitude,
                        longitude: longitude,
                        phone: null 
                    })
                    const Obj = { 
                        text: 'Location',
                        location : location,
                        contentType: 0,
                    }
                    Object.assign(this.messages,Obj)
                    this._sendMessage(this.messages,'Location');
                } catch (err) {
                    this._sendMessage(this.messages,'Not Found');
                }
            } else {
                this._sendMessage(this.messages,'Location Not Found , Maybe di dalem goa');
            }
        })
        return;
    }

    async rechecks() {
        let rec = await this.recheck(this.checkReader,this.messages.to);
        const mentions = await this.mention(rec);
        this.messages.contentMetadata = mentions.cmddata;
        await this._sendMessage(this.messages,mentions.names.join('')+

"↔️↔️↔️️↔️^ Sider Tercyduk Bangsat^↔️↔️↔️↔️"+
"Ketik Clear Untuk Hapus List Sider Ngentot");
        return;
    }

    infokick() {
                    this._sendMessage(this.messages, `┏━━━━ೋ• ❄ •ೋ━━━━━┓
    ❁Command Fitur Kickall❁   
┗━━━━ೋ• ❄ •ೋ━━━━━┛
1. Ketik Kick on
2. Jika Status Sudah Seperti Ini >"cancel":0,"kick":1 Berarti Kickall Sudah Siap Digunakan.
3. Terakhir, Lu Ketik Kickall (Gak Pake Spasi)
4. Done Dan Grup Rata~`);
     }


    async kickAll() {
        let groupID;
        if(this.stateStatus.kick == 1 && this.isAdminOrBot(this.messages.from)) {
            let target = this.messages.to;
            if(this.payload.length > 0) {
                let [ groups ] = await this._findGroupByName(this.payload.join(' '));
                groupID = groups.id;
            }
            let { listMember } = await this.searchGroup(groupID || target);
            for (var i = 0; i < listMember.length; i++) {
                if(!this.isAdminOrBot(listMember[i].mid)){
                    this._kickMember(groupID || target,[listMember[i].mid])
                }
            }
            return;
        } 
        return this._sendMessage(this.messages, ' Kick Error, Fitur Kick Hanya Untuk Admin Saja Ngentot!');
    }
    

    help() {
           this._sendMessage(this.messages, `┏━━━━ೋ• ❄ •ೋ━━━━━┓
    ❁Command For Selfbot❁   
┗━━━━ೋ• ❄ •ೋ━━━━━┛

┏━━━━ೋ• ❄ •ೋ━━━━━┓
║          Command admin
┗━━━━ೋ• ❄ •ೋ━━━━━┛
[🐯] Kick On/Off
[🐯] Kickall 
[🐯] Info kick 
[🐯] Cancel On/Off 
[🐯] Cancelall 
[🐯] Qrp On/Off 
┏━━━━ೋ• ❄ •ೋ━━━━━┓
║      Command In Group
┗━━━━ೋ• ❄ •ೋ━━━━━┛
[🐯] Bot keluar
[🐯] Status
[🐯] Speed 
[🐯] Left NamaGroup 
[🐯] Setpoint/Set/Cctv 
[🐯] Recheck/Check 
[🐯] Clear/Reset 
[🐯] Myid 
[🐯] Ig Ursname Kamu 
[🐯] Qr Open/Close 
[🐯] spam (S Kecil)
[🐯] List admin 
[🐯] Tag all
[🐯] Creator
[🐯] Suara bot1/bot2
[🐯] Media
┏━━━━ೋ• ❄ •ೋ━━━━━┓
☬         ❁Alphat Selfbot❁          ☬
┗━━━━ೋ• ❄ •ೋ━━━━━┛

━━━━━━━━━━━━━━━━━━━━┓
Support by: T҉̶̘̟̼̉̈́͐͋͌̊Σ̶Δ̶M҉̶̘͈̺̪͓̺ͩ͂̾ͪ̀̋ ̶̶̶N̶O̸͟͞O̶̶͓̽B̸͟͞ β̶Ω̶T҉̶̘̟̼̉̈́͐͋͌̊
━━━━━━━━━━━━━━━━━━━━┛`);
     }

    media() {
                    this._sendMessage(this.messages, `┏━━━━ೋ• ❄ •ೋ━━━━━┓
║          Keyword Media
┗━━━━ೋ• ❄ •ೋ━━━━━┛
[🐯] Pap tt/tete
[🐯] Pap naked
[🐯] Pap bugil
[🐯] Pap pocong
[🐯] Pap titid/tytyd
[🐯] Pap Kaget
[🐯] Pap tai/taik
[🐯] Pap kucing
[🐯] Pap anjing
┏━━━━ೋ• ❄ •ೋ━━━━━┓
║               Bot Music
┗━━━━ೋ• ❄ •ೋ━━━━━┛
[🐯] Musik funny
[🐯] Musik broken
[🐯] Musik siul
[🐯] Musik spongebob
[🐯] Musik simfoni
[🐯] Musik titanic
┏━━━━ೋ• ❄ •ೋ━━━━━┓
║             Daftar Lagu
┗━━━━ೋ• ❄ •ೋ━━━━━┛
[🐯] List lagu1
[🐯] List lagu2`);
     }

    listlagu1() {
                    this._sendMessage(this.messages, `┏━━━━ೋ• ❄ •ೋ━━━━━┓
║              List Lagu 1
┗━━━━ೋ• ❄ •ೋ━━━━━┛
[🎵] /lagu baby shark
[🎵] /lagu ML
[🎵] /lagu despacito
[🎵] /lagu faded
[🎵] /lagu dear god
[🎵] /lagu jadi aku sebentar saja
[🎵] /lagu mendua
[🎵] /lagu tentang rasa
[🎵] /lagu sayang
[🎵] /lagu jaran goyang
[🎵] /lagu goyang dumang`);
      }

    listlagu2() {
                    this._sendMessage(this.messages, `┏━━━━ೋ• ❄ •ೋ━━━━━┓
║              List Lagu 2
┗━━━━ೋ• ❄ •ೋ━━━━━┛
[🎵] /lagu asal kau bahagia
[🎵] /lagu canon rock
[🎵] /lagu closer
[🎵] /lagu dusk till dawn
[🎵] /lagu rockabye
[🎵] /lagu shape of you
[🎵] /lagu perfect
[🎵] /lagu hilang
[🎵] /lagu salah`);
      }

    gift() {
                    this._sendMessage(this.messages, `┏━━━━ೋ• ❄ •ೋ━━━━━┓
║                STICKER
┗━━━━ೋ• ❄ •ೋ━━━━━┛
[🎉] Gift sticker 1
[🎉] Gift sticker 2
[🎉] Gift sticker 3
[🎉] Gift sticker 4
┏━━━━ೋ• ❄ •ೋ━━━━━┓
║                THEMA
┗━━━━ೋ• ❄ •ೋ━━━━━┛
[🎉] Gift tema 1
[🎉] Gift tema 2
[🎉] Gift tema 3
[🎉] Gift tema 4`);
      }
      
    agler() {
                    this._sendMessage(this.messages, `Agler? Orang Ganteng Itu Mah Sehun Aja Kalah Gantengnya Sama Dia.`);
      }      


    async checkIG() {
        try {
            let { userProfile, userName, bio, media, follow } = await this._searchInstagram(this.payload[0]);
            await this._sendFileByUrl(this.messages,userProfile);
            await this._sendMessage(this.messages, `${userName}\n\nBIO:\n${bio}\n\n\uDBC0 ${follow} \uDBC0`)
            if(Array.isArray(media)) {
                for (let i = 0; i < media.length; i++) {
                    await this._sendFileByUrl(this.messages,media[i]);
                }
            } else {
                this._sendMessage(this.messages,media);
            }
        } catch (error) {
            this._sendMessage(this.messages,`Error: ${error}`);
        }
        return;
    }

    async textMessage(messages) {
        this.messages = messages;
        let payload = (this.messages.text !== null) ? this.messages.text.split(' ').splice(1).join(' ') : '' ;
        let receiver = messages.to;
        let sender = messages.from;
        
this.command('Halo', ['halo disini dengan kang coli','anda siapa?']);
this.command('Hi', ['hi disini dengan kang coli','kamu siapa?']);

this.command('Kang coli', ['Apa tot? ada yg bisa dibantu?','Apaan anj?']);

this.command('Pagi', ['pagi juga tot']);
this.command('Morning', ['morning juga tot!']);

this.command('Siang', ['siang juga tot','udah makan apa blum? udah berasil mupon apa belu!?']);
this.command('Siank', ['siang juga tot!']);

this.command('Sore', ['sore juga tot','sono tong mandi, lu bau bangsat']);

this.command('Malam', ['emank malam anj','yg bilang ini msih pagi siapa anj?']);
this.command('Night', ['emank malam sayang.. ','yg bilang ini msih pagi siapa NGENTOT!?']);

this.command('Bot lemot', ['bot bot banyak bacot lu NGENTOT!']);

this.command('@bye', ['paansi lu mau main usir aja gua human setengah bot NGENTOT! ']);
this.command('Bye', ['byeee, semoga tidak suka coli lagi! ']);

this.command('Itu bot', ['Iya knp emang anj!?']);
this.command('Itu bot?', ['Iya knp emang anj!?']);
this.command('Itu bot??', ['Iya knp emang anj!?']);

this.command('Bot peak', ['BACOT LU NGENTOT']);
this.command('Bot pea', ['BACOT LU NGENTOT']);

this.command('Beb', ['dih dasar jones!']);
this.command('Sayang', ['dih paansi!?']);

this.command('Wc', ['Semoga betah tong']);

this.command('Bot', ['Bacot lu ngentot?']);











this.command('halo', ['halo disini dengan kang coli','anda siapa?']);
this.command('hi', ['hi disini dengan kang coli','kamu siapa?']);

this.command('Kang coli', ['Apa tot? ada yg bisa dibantu?']);

this.command('pagi', ['pagi juga tot']);
this.command('morning', ['morning juga tot!']);

this.command('siang', ['siang juga tot','udah makan apa blum? udah berasil mupon apa belu!?']);
this.command('siank', ['siang juga tot!']);

this.command('sore', ['sore juga tot','sono tong mandi, lu bau bangsat']);

this.command('malam', ['emank malam anj','yg bilang ini msih pagi siapa anj?']);
this.command('night', ['emank malam kak','yg bilang ini msih pagi spa kak!?']);

this.command('bot lemot', ['jaringan lu yg lemot kontol']);

this.command('bye', ['Selamat jalan~ semoga tidak coli lagi~']);

this.command('itu bot', ['Iya knp emang anj!?']);
this.command('itu bot?', ['Iya knp emang anj!?']);
this.command('itu bot??', ['Iya knp emang anj!?']);

this.command('bot peak', ['BACOT LU NGENTOT']);
this.command('bot pea', ['BACOT LU NGENTOT']);

this.command('beb', ['dih dasar jones!']);
this.command('sayang', ['dih paansi!?']);

this.command('wc', ['Semoga betah tong']);

this.command('bot', ['Bacot lu ngentot?']);



        this.command('Siapa kamu', this.getProfile.bind(this));
        this.command('Status', `Your Status: ${JSON.stringify(this.stateStatus)}`);
        this.command(`Left ${payload}`, this.leftGroupByName.bind(this));
        this.command('Speed', this.getSpeed.bind(this));
        this.command('Kernel', this.checkKernel.bind(this));
        this.command(`Kick ${payload}`, this.OnOff.bind(this));
        this.command(`Cancel ${payload}`, this.OnOff.bind(this));
        this.command(`Qrp ${payload}`, this.OnOff.bind(this));
        this.command(`Kickall ${payload}`,this.kickAll.bind(this));
        this.command(`Cancelall ${payload}`, this.cancelMember.bind(this));
        this.command(`Set`,this.setReader.bind(this));
        this.command(`set`,this.setReader.bind(this));
        this.command(`Cctv`,this.setReader.bind(this));
        this.command(`cctv`,this.setReader.bind(this));
        this.command(`Setpoint`,this.setReader.bind(this));
        this.command(`setpoint`,this.setReader.bind(this));
        this.command(`Recheck`,this.rechecks.bind(this));
        this.command(`recheck`,this.rechecks.bind(this));
        this.command(`Check`,this.rechecks.bind(this));
        this.command(`check`,this.rechecks.bind(this));
        this.command(`Cyduk`,this.rechecks.bind(this));
        this.command(`cyduk`,this.rechecks.bind(this));
        this.command(`Ciduk`,this.rechecks.bind(this));
        this.command(`ciduk`,this.rechecks.bind(this));
        this.command(`Check sider`,this.rechecks.bind(this));
        this.command(`Clearall`,this.clear.bind(this));
        this.command(`Clear`,this.clear.bind(this));
        this.command(`clear`,this.clear.bind(this));
        this.command(`Reset`,this.clear.bind(this));
        this.command(`reset`,this.clear.bind(this));
        this.command('Myid',`Your ID: ${messages.from}`)
        this.command(`ip ${payload}`,this.checkIP.bind(this))
        this.command(`Ig ${payload}`,this.checkIG.bind(this))
        this.command(`Qr ${payload}`,this.qrOpenClose.bind(this))
        this.command(`Joinqr ${payload}`,this.joinQr.bind(this));
        this.command(`spam ${payload}`,this.spam2.bind(this));
        this.command(`Spamgroup ${payload}`,this.spamGroup.bind(this));
        this.command(`Creator`,this.creator.bind(this));
        this.command(`List admin`,this.list.bind(this));
        this.command(`Admin1`,this.admin1.bind(this));
        this.command(`Admin2`,this.admin2.bind(this));
        this.command(`Admin3`,this.admin3.bind(this));
        this.command(`Admin4`,this.admin4.bind(this));
        this.command(`Admin5`,this.admin5.bind(this));
        this.command(`Admin6`,this.admin6.bind(this));
        this.command(`Admin7`,this.admin7.bind(this));
        this.command(`Admin8`,this.admin8.bind(this));
        this.command(`Admin9`,this.admin9.bind(this));
        this.command(`Admin10`,this.admin10.bind(this));
        this.command(`Admin11`,this.admin11.bind(this));
        this.command(`Admin12`,this.admin12.bind(this));
        this.command(`Admin13`,this.admin13.bind(this));
        this.command(`Admin14`,this.admin14.bind(this));
        this.command(`Admin15`,this.admin15.bind(this));
        this.command(`Pap ${payload}`,this.searchLocalImage.bind(this));
        this.command(`Upload ${payload}`,this.prepareUpload.bind(this));
        this.command(`Musik ${payload}`,this.vn.bind(this));
        this.command(`Suara ${payload}`,this.vn.bind(this));
        this.command(`/lagu ${payload}`,this.lagu.bind(this));
        this.command(`Video ${payload}`,this.video.bind(this));
        this.command(`Tag all`,this.tagall.bind(this));
        this.command(`Tagall`,this.tagall2.bind(this));
        this.command(`Help`,this.help.bind(this));
        this.command(`help`,this.help.bind(this));
        this.command(`Menu`,this.help.bind(this));
        this.command(`/help`,this.help.bind(this));
        this.command(`Keyword`,this.help.bind(this));
        this.command(`Key`,this.help.bind(this));
        this.command(`Info kick`,this.infokick.bind(this));
        this.command(`List lagu1`,this.listlagu1.bind(this));
        this.command(`List lagu2`,this.listlagu2.bind(this));
        this.command(`List lagu 1`,this.listlagu1.bind(this));
        this.command(`List lagu 2`,this.listlagu2.bind(this));
        this.command(`Bot keluar`,this.keluar.bind(this));
        this.command(`#tidak`,this.batal.bind(this));
        this.command(`Gift`,this.gift.bind(this));
        this.command(`gift`,this.gift.bind(this));
        this.command(`Media`,this.media.bind(this));
        this.command(`Bot2`,this.bot2.bind(this));


        if(messages.contentType == 13) {
            messages.contentType = 0;
            if(!this.isAdminOrBot(messages.contentMetadata.mid)) {
                this._sendMessage(messages,messages.contentMetadata.mid);
            }
            return;
        }



  if (messages.text == `Wkwkwk`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '100',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `wkwkwk`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '100',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Wkwk`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '100',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `wkwk`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '100',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Hahaha`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '100',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `hahaha`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '100',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Haha`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '100',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `haha`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '100',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Hehe`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '10',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `hehe`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '10',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Hehehe`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '10',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `hehehe`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '10',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Galau`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '9',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `galau`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '9',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `You`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '7',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `you`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '7',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Kau`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '7',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `kau`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '7',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Kamu`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '7',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `kamu`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '7',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Hadeuh`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '6',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `hadeuh`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '6',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Hadeh`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '6',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `hadeh`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '6',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Marah`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '6',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `marah`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '6',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Please`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '4',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `please`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '4',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Tolong`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '4',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `tolong`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '4',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Pliss`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '4',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `pliss`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '4',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Haaa`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '3',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `haaa`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '3',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Kaget`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '3',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `kaget`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '3',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Lol`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '110',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `LoL`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '110',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `LOL`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '110',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
      }

  if (messages.text == `Ngakak`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '110',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `ngakak`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '110',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Lucu`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '110',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `lucu`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '110',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Hmm`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '101',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `hmm`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '101',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Hmmm`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '101',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `hmmm`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '101',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Welcome`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '247',
                                    'STKPKGID': '3',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `welcome`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '247',
                                    'STKPKGID': '3',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Tidur`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '1',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `tidur`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '1',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Gemes`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '1',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `gemes`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '2',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Cantik`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '5',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `cantik`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '5',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Nyanyi`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '11',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `nyanyi`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '11',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Lalala`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '11',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Gugup`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '8',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `gugup`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '8',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Ok`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `ok`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Oke`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `oke`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Okay`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `okay`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Oce`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `oce`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Okee`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `okee`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Sip`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `sip`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Siph`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `siph`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '13',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Mantap`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '14',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `mantap`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '14',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Nice`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '14',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `nice`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '14',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Keren`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '14',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `keren`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '14',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }


  if (messages.text == `Ngejek`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '15',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `ngejek`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '15',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Sedih`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '16',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `sedih`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '16',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Nangis`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '16',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `nangis`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '16',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Kampret`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '102',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `kampret`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '102',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Woi`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '102',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `woi`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '102',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `Huft`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '104',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }

  if (messages.text == `huft`){
        messages.contentType = 0;
       this._sendMessage(messages, "sent sticker",messages.contentMetadata={'STKID': '104',
                                    'STKPKGID': '1',
                                    'STKVER': '100'},messages.contentType=7);
     }



  if (messages.text == 'Gift tema 1'){
        messages.contentType = 0;
       this._sendMessage(messages, "gift sent",messages.contentMetadata={'PRDID': 'a0768339-c2d3-4189-9653-2909e9bb6f58',
                                    'PRDTYPE': 'THEME',
                                    'MSGTPL': '6'},messages.contentType=9);
     }

  if (messages.text == 'Gift tema 2'){
        messages.contentType = 0;
       this._sendMessage(messages, "gift sent",messages.contentMetadata={'PRDID': 'ec4a14ea-7437-407b-aee7-96b1cbbc1b4b',
                                    'PRDTYPE': 'THEME',
                                    'MSGTPL': '5'},messages.contentType=9);
     }

  if (messages.text == 'Gift tema 3'){
        messages.contentType = 0;
       this._sendMessage(messages, "gift sent",messages.contentMetadata={'PRDID': 'd4f09a5f-29df-48ac-bca6-a204121ea165',
                                    'PRDTYPE': 'THEME',
                                    'MSGTPL': '7'},messages.contentType=9);
     }

  if (messages.text == 'Gift tema 4'){
        messages.contentType = 0;
       this._sendMessage(messages, "gift sent",messages.contentMetadata={'PRDID': '25e24851-994d-4636-9463-597387ec7b73',
                                    'PRDTYPE': 'THEME',
                                    'MSGTPL': '8'},messages.contentType=9);
     }



  if (messages.text == 'Gift sticker 1'){
        messages.contentType = 0;
       this._sendMessage(messages, "gift sent",messages.contentMetadata={'STKPKGID': '9778',
                                    'PRDTYPE': 'STICKER',
                                    'MSGTPL': '1'},messages.contentType=9);
     }

  if (messages.text == 'Gift sticker 2'){
        messages.contentType = 0;
       this._sendMessage(messages, "gift sent",messages.contentMetadata={'STKPKGID': '1699',
                                    'PRDTYPE': 'STICKER',
                                    'MSGTPL': '2'},messages.contentType=9);
     }

  if (messages.text == 'Gift sticker 3'){
        messages.contentType = 0;
       this._sendMessage(messages, "gift sent",messages.contentMetadata={'STKPKGID': '1073',
                                    'PRDTYPE': 'STICKER',
                                    'MSGTPL': '3'},messages.contentType=9);
     }

  if (messages.text == 'Gift sticker 4'){
        messages.contentType = 0;
       this._sendMessage(messages, "gift sent",messages.contentMetadata={'STKPKGID': '1405277',
                                    'PRDTYPE': 'STICKER',
                                    'MSGTPL': '4'},messages.contentType=9);
     }



  if (messages.text == '#ya'){
               await this._sendMessage(messages,'Kakak Jahat :( Lain X Jangan Invite Aku Lagi -,-');
     {
this._leaveGroup(this.messages.to);
     }
     }

        if(this.stateUpload.group == messages.to && [1,2,3].includes(messages.contentType)) {
            if(sender === this.stateUpload.sender) {
                this.doUpload(messages);
                return;
            } else {
                messages.contentType = 0;
                this._sendMessage(messages,'Wrong Sender !! Reseted');
            }
            this.resetStateUpload();
            return;
        }

        // if(cmd == 'Lirik ') {
        //     let lyrics = await this._searchLyrics(payload);
        //     this._sendMessage(seq,lyrics);
        // }

    }

}


module.exports = new LINE();